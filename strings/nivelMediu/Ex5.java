package strings.nivelMediu;

public class Ex5 {
    public static void main(String[] args) {
        String words = "Java e cel mai tare limbaj"; // 6 cuvinte
        int cuvinte = 0;
        for (int i = 0; i < words.length(); i++) {
            if (words.charAt(i) == ' ') {
                cuvinte++;
            }
        }
        cuvinte++; // numarul de cuvinte este de fapt numarul de spatii + 1
        System.out.println(cuvinte);
    }
}
