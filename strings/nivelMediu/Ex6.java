package strings.nivelMediu;

public class Ex6 {
    public static void main(String[] args) {
        String a = "mama ce iti fac eu tie"; // uigres e icia
        String resultat = "";
        int cuvinte = 0;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == ' ') {
                cuvinte++;
            }
        }
        cuvinte++;
        for (int i = 0; i < cuvinte - 1; i++) {
            String word = a.substring(0, a.indexOf(' '));

            String reversed = getReversed(word);
            resultat = resultat + ' ' + reversed;
            a = a.substring(a.indexOf(' ') + 1);

        }
        String lastWordReversed = getReversed(a);
        resultat = resultat + ' ' + lastWordReversed;
        resultat = resultat.substring(1); // elimin primul spatiu gol, pt ca primul char e spatiu
        System.out.println(resultat);

    }

    private static String getReversed(String word) {
        String reversed = "";
        for (int j = 0; j < word.length(); j++) {
            reversed = word.charAt(j) + reversed;
        }
        return reversed;
    }
}
