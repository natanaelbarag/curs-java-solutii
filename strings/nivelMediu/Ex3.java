package strings.nivelMediu;

public class Ex3 {
    public static void main(String[] args) {
        String words = "Eu sunt programator";
        words = words.toLowerCase(); // ca sa evit verificarea si cu literele mari.
        int vocale = 0;
        int cons = 0;
        for (int i = 0; i < words.length(); i++) {
            char caracter = words.charAt(i);
            if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
                vocale++;
            } else {
                cons++;
            }
        }
        System.out.println("Vocale: " + vocale);
        System.out.println("Consoane : " + cons);
    }
}
