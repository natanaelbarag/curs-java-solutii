package strings.nivelMediu;

public class Ex9 {
    public static void main(String[] args) {
        String a = "ana are mere";
        String res = "";
        boolean isFirst = true;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == 'a' && isFirst == true) {
                res = res + a.charAt(i);
                isFirst = false;
            } else if (a.charAt(i) == 'a') {
                res = res + '$';
            } else {
                res = res + a.charAt(i);
            }

        }
        System.out.println(res);
    }
}
