package strings.nivelUsor;

public class ToateProblemele {
    public static void main(String[] args) {
        String a = "ana are mere";
        //1
        System.out.println(a.length());

        //2
        int n = 3;
        System.out.println(a.charAt(n));

        //3
        String sectiune = "are";
        System.out.println(a.indexOf(sectiune));

        //4
        String c = "hei";
        String b = "ieh";
        if (c.length() > b.length()) {
            System.out.println("c e mai mare");
        } else {
            System.out.println(" b e mai mare");
        }

        //5
        System.out.println(a.toUpperCase());

        //6
        System.out.println(a.toLowerCase());

        //7
        System.out.println(b.concat(c));

        //8
        String prop = "Programarea e tare";
        String sec = "tare";
        if (prop.contains(sec)) {
            System.out.println("DA");
        } else {
            System.out.println("NU");
        }


        // 9
        if (a.endsWith(".")) {
            System.out.println("a se termina cu .");
        } else {
            System.out.println("a nu se termina cu .");
        }

        // 10
        if (b.equals(c)) {
            System.out.println("a si b au aceeasi valoare");
        } else {
            System.out.println("b si c nu au aceeasi valoare");
        }

        //11
        char x = 'b';
        if (a.contains(String.valueOf(x))) {
            System.out.println(x + " este in " + a);
        } else {
            System.out.println(x + " nu este in " + a);
        }

        //12
        if (a.startsWith("a") && a.endsWith("b")) {
            System.out.println("DA");
        } else {
            System.out.println("NU");
        }

        //13
        System.out.println("--------------");
        String cuvinte = "Cele trei cuvinte";
        String ultimulCuvant = cuvinte.substring(cuvinte.lastIndexOf(' '));
        System.out.println("Ultimul cuvant are : " + (ultimulCuvant.length() - 1));

        // 14
        // lucram tot cu 'cuvinte'
        cuvinte = cuvinte.substring(0, cuvinte.lastIndexOf(' '));

        String penultimulCuvant = cuvinte.substring(cuvinte.lastIndexOf(' '));
        System.out.println("Penultimul cuvant are : " + (penultimulCuvant.length() - 1));

        //15

        String d = "Imi place programarea foarte tare";
        //programarea =>
        d = d.substring(d.indexOf(' ') + 1); // scap de primul cuvant
        d = d.substring(d.indexOf(' ') + 1); // scap tot de primul cuvant, doar ca de aceasta data este al doilea cuvant
        System.out.println("Al treilea cuvant are " + d.indexOf(' '));

    }
}
