package repetitive.maximeMinime;

import java.util.Scanner;

public class ex2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 3;
        int min = -1;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            if (min == -1 || a < min) {
                min = a;
            }
        }
        System.out.println(min);
    }
}
