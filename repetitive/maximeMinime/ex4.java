package repetitive.maximeMinime;

import java.util.Scanner;

public class ex4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int max = n;
        while (n != 0) {
            n = scanner.nextInt();
            if (n > max) {
                max = n;
            }

        }
        System.out.println(max);
    }
}
