package repetitive.maximeMinime;

import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 10;
        int max = 0;
        int min = -1;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            if (a > max) {
                max = a;
            }
            if (min == -1 || a < min) {
                min = a;
            }
        }
        System.out.println(min + max);
    }
}
