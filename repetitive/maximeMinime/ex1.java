package repetitive.maximeMinime;

import java.util.Scanner;

public class ex1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 3;
        int max = 0;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            if (a > max) {
                max = a;
            }
        }
        System.out.println(max);
    }
}
