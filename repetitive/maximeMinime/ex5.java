package repetitive.maximeMinime;

import java.util.Scanner;

public class ex5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int min = n;
        while (n != 0) {
            n = scanner.nextInt();
            if (n < min) {
                min = n;
            }

        }
        System.out.println(min);
    }
}
