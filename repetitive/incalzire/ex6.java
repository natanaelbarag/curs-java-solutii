package repetitive.incalzire;

public class ex6 {
    public static void main(String[] args) {
        // trebuie sa se poata ridica n la m
        int n = 2;
        int m = 16;
        int contor = 0;
        while (m / n != 0) {
            contor++;
            m = m / n;
        }
        System.out.println(contor);
    }
}
