package repetitive.incalzire;

public class ex12 {
    public static void main(String[] args) {
        int n = 17;
        boolean prim = true;
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                prim = false;
            }
        }
        if (prim == true) {
            System.out.println("Numarul este prim");
        } else {
            System.out.println("Numarul nu este prim");
        }
    }
}
