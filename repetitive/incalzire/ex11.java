package repetitive.incalzire;

public class ex11 {
    public static void main(String[] args) {
        int n = 12;
        int sum = 0;
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                sum = sum + i;
            }
        }
        System.out.println(sum);
    }
}
