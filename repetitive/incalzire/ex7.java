package repetitive.incalzire;

import java.util.Scanner;

public class ex7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 5;
        int contor = 0;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            for (int j = 2; j < a / 2; j++) {
                if (j * j == a) {
                    contor++;
                }
            }

        }
        System.out.println(contor);
    }
}
