package repetitive.sumeProduseNumarari;

public class ex5 {
    public static void main(String[] args) {
        int n = 563663;
        int copyN = n;
        int max = n % 10;
        while (n > 0) {
            if (n % 10 > max) {
                max = n % 10;
            }
            n = n / 10;
        }
        int cont = 0;

        while (copyN > 0) {
            if (copyN % 10 == max) {
                cont++;
            }
            copyN = copyN / 10;
        }
        System.out.println(cont);
    }
}
