package repetitive.sumeProduseNumarari;

public class ex2 {
    public static void main(String[] args) {
        int x = 34632;
        int c = 3;
        int cont = 0;
        while (x > 0) {
            if (x % 10 == c) {
                cont++;
            }
            x /= 10;
        }
        System.out.println(cont);
    }
}
