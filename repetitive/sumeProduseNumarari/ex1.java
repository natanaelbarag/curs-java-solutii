package repetitive.sumeProduseNumarari;

public class ex1 {
    public static void main(String[] args) {
        int n = 4363;
        int pare = 0;
        int impare = 0;
        while (n > 0) {
            if (n % 10 % 2 == 0) {
                pare++;
            } else {
                impare++;
            }
            n /= 10;
        }
        System.out.println("Pare : " + pare);
        System.out.println("Impare :  " + impare);
    }
}
