package repetitive.sumeProduseNumarari;

public class ex4 {
    public static void main(String[] args) {
        int n = 5633;
        int min = n % 10;
        while (n > 0) {
            if (n % 10 < min) {
                min = n % 10;
            }
            n = n / 10;
        }
        System.out.println(min);
    }
}
