package repetitive.sumeProduseNumarari;

import java.util.Scanner;

public class ex7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 3;
        int pare = 0;
        int impare = 0;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();

            while (a > 0) {
                if (a % 2 == 0) {
                    pare++;
                } else {
                    impare++;
                }
                a /= 10;
            }
        }
        System.out.println("Pare: " + pare);
        System.out.println("Impare : " + impare);

    }
}
