package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex1 {
    public static void main(String[] args) {
        int n = 5;
        int c = 0;
        Scanner scanner = new Scanner(System.in);
        for (int i = 1; i <= n; i++) {
            int nr = scanner.nextInt();
            int max = nr % 10;
            while (nr > 0) {
                if (nr % 10 > max && nr % 10 % 2 == 1) {
                    max = nr % 10;
                }
                nr = nr / 10;
            }

            if (max % 2 == 1) {
                c++;
            }
        }
        System.out.println(c);
    }
}
