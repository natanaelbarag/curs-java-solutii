package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex36 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 3;
        int suma = 0;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            int uca = a % 10;
            int nou = 0;
            int p = 1;
            while (a > 0) {
                if (a % 10 != uca) {
                    nou = (a % 10 * p) + nou;
                    p = p * 10;
                }
                a /= 10;

            }
            suma += nou;
        }
        System.out.println(suma);
    }
}
