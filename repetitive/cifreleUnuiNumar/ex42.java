package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex42 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = 4;
        boolean found = false;
        int primPar = -1;
        for (int i = 1; i <= n; i++) {
            int a = scanner.nextInt();
            if (found == false && a % 2 == 0) {
                primPar = a;
                found = true;
            }
        }
        if (primPar == -1) {
            System.out.println("Nu au fost numere pare");
        } else {
            System.out.println(primPar);

        }

    }
}
