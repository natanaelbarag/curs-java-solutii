package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex41 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = 4;
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            int a = scanner.nextInt();
            if (a % 2 == 0) {
                sum = sum + a;
            }
        }
        System.out.println("Media aritmetica : " + (sum / n));
    }
}
