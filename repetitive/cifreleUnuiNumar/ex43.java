package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex43 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = 4;
        int ultimPar = -1;
        for (int i = 1; i <= n; i++) {
            int a = scanner.nextInt();
            if (a % 2 == 0) {
                ultimPar = a;

            }
        }
        if (ultimPar == -1) {
            System.out.println("Nu au fost numere pare");
        } else {
            System.out.println(ultimPar);

        }
    }
}
