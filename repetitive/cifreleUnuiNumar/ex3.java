package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int sumaPatratelorCifrelor = 0;
        int sumaImpare = 0;
        int sumaDivizibileCu3 = 0;
        int numarulCifrelorMaiMariDecat5 = 0;
        int prodCifNenuleN = 1;
        int sumaUnitatiSiPrimaCifra = n % 10;

        while (n > 0) {
            int uc = n % 10;
            sumaPatratelorCifrelor += uc * uc;

            if (uc % 2 == 1) {
                sumaImpare += uc;
            }
            if (uc % 3 == 0) {
                sumaDivizibileCu3 += uc;
            }
            if (uc > 5) {
                numarulCifrelorMaiMariDecat5 += uc;
            }
            if (uc != 0) {
                prodCifNenuleN *= uc;
            }

            if (n < 10) {
                sumaUnitatiSiPrimaCifra += n;
            }
        }
    }
}
