package repetitive.cifreleUnuiNumar;

public class ex2 {
    public static void main(String[] args) {
        int n = 123;
        int k = 6;
        boolean maiMici = true;
        while (n > 0) {
            if (n % 10 >= k) {
                maiMici = false;
            }
            n = n / 10;
        }

        if (maiMici == true) {
            System.out.println("Da, toate sunt mai mici decat k");
        } else {
            System.out.println("Nu toate sunt mai mici decat k");
        }
    }
}
