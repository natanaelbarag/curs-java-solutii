package repetitive.cifreleUnuiNumar;

import java.util.Scanner;

public class ex40 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int contorImpare = 0;
        int n = scanner.nextInt();
        while (n != 0) {
            if (n % 2 != 0) {
                contorImpare++;
            }
            n = scanner.nextInt();
        }
        System.out.println(contorImpare);
    }
}
