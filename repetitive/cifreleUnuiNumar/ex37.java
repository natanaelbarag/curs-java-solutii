package repetitive.cifreleUnuiNumar;

public class ex37 {
    public static void main(String[] args) {
        int n = 99;
        int sumaCif = 0;
        while (n > 0) {
            sumaCif = sumaCif + n % 10;
            n /= 10;
        }
        int oglindit = 0;
        while (sumaCif > 0) {
            oglindit = oglindit * 10 + sumaCif % 10;
            sumaCif /= 10;
        }

        System.out.println(oglindit);
    }
}
