package repetitive.divizori;

public class ex2 {
    public static void main(String[] args) {
        int n = 45;
        int cont = 0;
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                cont++;
            }
        }
        if (cont % 2 == 1) {
            System.out.println("Are nr impar de divizori");
        } else {
            System.out.println("Nu are nr impar de divizori");
        }
    }
}
