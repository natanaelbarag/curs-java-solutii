package repetitive.divizori;

import java.util.Scanner;

public class ex7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sumDiv = 0;
        int contDiv = 0;
        for (int i = 1; i <= n / 2; i++) {
            if (n % 1 == 0) {
                sumDiv = sumDiv + i;
                contDiv++;
            }
        }
        System.out.println("Media aritmetica : " + (sumDiv / contDiv));
    }
}
