package repetitive.divizori;

import java.util.Scanner;

public class ex8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int oglindit = 0;
        while (n > 0) {
            oglindit = oglindit * 10 + n % 10;
            n /= 10;
        }
        int cont = 0;
        for (int d = 2; d <= oglindit / 2; d++) {
            if (oglindit % d == 0) {
                cont++;
            }
        }
        System.out.println(cont);
    }
}
