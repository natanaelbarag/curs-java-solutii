package repetitive.divizori;

public class ex4 {
    public static void main(String[] args) {
        int n = 45;
        int suma = 0;
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 1) {
                suma++;
            }
        }
        System.out.println(suma);
    }
}
