package repetitive.divizori;

public class ex6 {
    public static void main(String[] args) {
        int n = 45;
        int suma = 0;
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                suma++;
            }
        }
        System.out.println(suma);
    }
}
