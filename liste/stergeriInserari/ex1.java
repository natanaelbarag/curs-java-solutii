package liste.stergeriInserari;

import java.util.Arrays;

public class ex1 {
    public static void main(String[] args) {
        // 1. Să se șteargă dintr-un șir elementul aflat pe o poziție dată
        int[] lista = {2, 3, 4, 5, 65, 4};
        int[] listaNoua = new int[lista.length - 1];
        int pozitie = 0;
        int k = 0;
        for (int i = 0; i < lista.length; i++) {
            if (i != pozitie) {
                listaNoua[k] = lista[i];
                k++;
            }
        }
        System.out.println(Arrays.toString(listaNoua));
    }
}