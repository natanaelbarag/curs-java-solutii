package liste.stergeriInserari;

import java.util.Arrays;

public class ex3 {
    public static void main(String[] args) {
        // 3. Să se insereze pe o poziție dată într-un șir o valoare precizată.
        int[] lista = {1, 3, 3, 4, 5};
        int[] listaNoua = new int[lista.length + 1];
        int pozitie = 5;
        int inserareValoare = 523;
        for (int i = 0; i < lista.length + 1; i++) {
            if (i < pozitie - 1)
                listaNoua[i] = lista[i];
            else if (i == pozitie - 1)
                listaNoua[i] = inserareValoare;
            else
                listaNoua[i] = lista[i - 1];
        }
        System.out.println(Arrays.toString(listaNoua));
    }
}
